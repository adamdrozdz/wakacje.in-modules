"use strict";

import { Feature } from 'ol';
import { fromLonLat } from 'ol/proj';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import { LineString } from 'ol/geom';
import { Style, Stroke, Fill } from 'ol/style';
export { flyTo, createEmptyVectorLayer, drawCountryBoundary };

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function flyTo(nominatimFormatBbox) {
  var bbox = JSON.parse(nominatimFormatBbox);
  var p1 = fromLonLat([+bbox[2], +bbox[0]]);
  var p2 = fromLonLat([+bbox[3], +bbox[1]]);
  var extent = [].concat(_toConsumableArray(p1), _toConsumableArray(p2));
  this.getView().fit(extent, {
    size: this.getSize(),
    maxZoom: 10,
    duration: 400
  });
}

function createEmptyVectorLayer(name) {
  var source = new VectorSource({});
  var layer = new VectorLayer({ source: source });
  layer.set('type', name);
  this.addLayer(layer);
}

function drawCountryBoundary(boundary, layer) {
  var color = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'blue';
  var strokeWidth = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 1;
  var mappedCoordinates = boundary.map(function (coordinate) {
    return fromLonLat(coordinate);
  });
  var geometry = new LineString(mappedCoordinates);
  var feature = new Feature({ geometry: geometry });
  feature.setStyle(new Style({
    stroke: new Stroke({
      color: color,
      width: strokeWidth
    }),
    fill: new Fill({
      color: 'rgba(0, 0, 255, 0.1)'
    })
  }));
  var source = layer.getSource();
  source.clear(true);
  source.addFeature(feature);
}
